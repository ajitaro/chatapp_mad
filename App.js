import React, {Component} from 'react';
import { createSwitchNavigator, createStackNavigator, createAppContainer } from "react-navigation";
import LoginScreen from "./screens/LoginScreen"
import HomeScreen from "./screens/HomeScreen"
import AuthLoadingScreen from "./screens/AuthLoadingScreen"
import ChatScreen from "./screens/ChatScreen"
import ProfileScreen from "./screens/ProfileScreen"
import initialPage from "./screens/initialPage"

const AppStack = createStackNavigator(
    {
        Home: HomeScreen,
        Chat: ChatScreen,
        Profile: ProfileScreen,
        Login: LoginScreen,
    }
);

const AuthStack = createStackNavigator({GetStarted: initialPage});

export default createAppContainer(createSwitchNavigator(
    {
        AuthLoading: AuthLoadingScreen,
        App: AppStack,
        Auth: AuthStack,
    },
    {
      initialRouteName: 'AuthLoading',
    }
));