import {StyleSheet} from "react-native";

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#F5FCFF',
    },
    input: {
        padding: 10,
        borderWidth: 1,
        borderColor: '#ccc',
        width: '70%',
        marginBottom: 10,
        borderRadius: 5
    },
    btnText: {
        fontSize: 20,
        color: '#CC4F41'
    },
    avatarIcon: {
        backgroundColor: '#111',
        height: 40,
        width: 40,
        borderRadius: 20,
        alignItems: 'center',
        justifyContent: 'center',
        marginRight: 10,
    },

});

export default styles