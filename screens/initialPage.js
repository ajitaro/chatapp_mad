import React, {Component} from 'react';
import {
    Platform,
    StyleSheet,
    Text,
    AsyncStorage,
    TouchableOpacity,
    TextInput,
    View,
    Image,
    Button
} from 'react-native';
import User from '../User'

export default class initialPage extends Component{
    static navigationOptions = {
        header: null
    }

    render(){
        return(
            <View
                style={{
                    flex: 1,
                    alignItems: 'center',
                    justifyContent: 'center'
                }}
            >
                <Image
                    source={require('../images/telegram_Logo.png')}
                    style={{
                        height: "22%",
                        width: "40%",
                        margin: 50,
                        resizeMode: 'contain'
                    }}
                />
                <Text
                    style={{
                        fontSize: 24,
                        paddingBottom: 5,
                    }}
                >
                    Telegram
                </Text>
                <Text>
                    The world's fastest messaging app.
                </Text>
                <Text
                    style={{
                        paddingBottom: 5,
                    }}
                >
                    It is free and secure
                </Text>
                <View
                    style={{
                        margin: 100,
                    }}
                >
                <Button
                    title={'START MESSAGING'}
                    onPress={() => this.props.navigation.navigate('Login')}
                />
                </View>
            </View>
        )
    }
}