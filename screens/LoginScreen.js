import React, {Component} from 'react';
import {
    Platform,
    StyleSheet,
    Text,
    AsyncStorage,
    TouchableOpacity,
    TextInput,
    View,
    Button
} from 'react-native';
import User from '../User'

import firebase from 'firebase'
import Entypo from "react-native-vector-icons/AntDesign";

const checkIcon = <Entypo name="check" size={20} color="#fff" />;
export default class loginScreen extends Component{
    static navigationOptions = ({navigation}) => {
        return {
            headerStyle: {
                backgroundColor: '#59A6DD'
            },
            headerTintColor: '#fff',
            headerRight: (
                <TouchableOpacity
                    style={{marginRight: 10}}
                    onPress={this.submitForm}>
                    {checkIcon}
                </TouchableOpacity>
            ),
            headerLeft: (
                <Text
                    style={{
                        color: "#fff",
                        fontWeight: 'bold',
                        fontSize: 20,
                        marginLeft: 10
                    }}
                >
                    Your Phone
                </Text>
            )
        }
    }

    state = {
        phone: '',
        name: '',
        avatar: '',
    }

    componentWillMount(){
        AsyncStorage.getItem('userPhone').then(val => {
            if(val){
                this.setState({phone:val});
            }
        })
    }

    handleChange = key => val => {
        this.setState({[key]:val})
    }

    submitForm = async () => {

        if(this.state.name == '' || this.state.phone == ''){
            alert('Enter your name and phone number')
        }else{
            await AsyncStorage.setItem('userPhone', this.state.phone)
            User.phone = this.state.phone
            firebase.database().ref('user/'+User.phone).set({name:this.state.name})

            this.props.navigation.navigate('Home')
            alert('Telepon sukses disimpan')
        }
    }

    render() {
        return (
            <View style={styles.container}>
                <TextInput
                    placeholder = "Indonesia"
                    style = {[{width: "90%"}, styles.input]}
                    value = {this.state.name}
                    onChangeText = {this.handleChange('name')}
                />
                <View
                    style={{
                        flexDirection: 'row'
                    }}
                >
                    <Text
                        style = {[{width: "15%"}, styles.input]}
                    >
                        +62
                    </Text>
                    <TextInput
                        placeholder = ""
                        style = {[{width: "70%"}, styles.input]}
                        value = {this.state.phone}
                        onChangeText = {this.handleChange('phone')}
                    />
                </View>
                <Text
                    style={{
                        margin: 10,
                    }}
                >
                    Please confirm your country code and enter your phone number.
                </Text>
                <Button
                    title='GO'
                    onPress={this.submitForm}>
                </Button>
            </View>
        );
    }

}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        paddingTop: 20,
        paddingHorizontal: 15,
        backgroundColor: '#F5FCFF'
    },
    input: {
        padding: 10,
        marginBottom: 10,
        borderBottomWidth: 1,
        borderBottomColor: "#ccc",
        marginHorizontal: "2%",
    }
})