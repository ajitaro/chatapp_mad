import React, {Component} from 'react';
import {
    SafeAreaView,
    Text,
    View,
    TextInput,
    Dimensions,
    FlatList,
    TouchableOpacity,
    Image,
    ImageBackground
} from 'react-native';
import User from '../User'
import styles from '../constants/styles'
import firebase from 'firebase'
import Entypo from 'react-native-vector-icons/Entypo'

const {height, width} = Dimensions.get('window')
const settingIcon = <Entypo name="dots-three-vertical" size={20} color="#fff" />;
export default class ChatScreen extends React.Component {
    static navigationOptions = ({navigation}) => {
        return{
            title:navigation.getParam('name', null),
            headerStyle: {
                backgroundColor: '#59A6DD'
            },
            headerTintColor: '#fff',
            headerRight: (
                <TouchableOpacity
                    style={{marginRight: 10}}
                    onPress={()=> navigation.navigate('Profile')}>
                    {settingIcon}
                </TouchableOpacity>
            ),
        }
    }

    constructor(props){
        super(props)
        this.state = {
            person: {
                name : props.navigation.getParam('name'),
                phone : props.navigation.getParam('phone')
            },
            textMessage: '',
            messageList: []
        }
    }

    componentWillMount(){
        console.log('haiii')
        firebase.database().ref('messages').child(User.phone).child(this.state.person.phone)
            .on('child_added', (value) => {
                this.setState((prevState) => {
                    return {
                        messageList: [...prevState.messageList, value.val()]
                    }
                })
            })
    }

    handleChange = key => val => {
        this.setState({[key]:val})
    }

    convertTime = (time) => {
        let d = new Date(time)
        let c = new Date();
        let result = (d.getHours()<10?'0':'')+d.getHours()+':';
        result += (d.getMinutes()<10?'0':''+d.getMinutes());
        if(c.getDay() !== d.getDay()){
            result = d.getDay()+' '+d.getMonth()+' '+result
        }
        return result
    }

    sendMessage = async () => {
        if(this.state.textMessage.length > 0){
            let msgId = firebase.database().ref('message').child(User.phone).child(this.state.person.phone).push().key
            let updates = {}
            let message = {
                message : this.state.textMessage,
                time: firebase.database.ServerValue.TIMESTAMP,
                from: User.phone
            }
            updates['messages/'+User.phone+'/'+this.state.person.phone+'/'+msgId] = message;
            updates['messages/'+this.state.person.phone+'/'+User.phone+'/'+msgId] = message;
            firebase.database().ref().update(updates)
            this.setState({textMessage:''})
        }
    }

    renderRow = ({item}) => {
        return(
            <View
                style={{
                    flexDirection: 'row',
                    alignSelf: item.from === User.phone ? 'flex-end':'flex-start',
                    backgroundColor: item.from === User.phone? '#c3edaf':'#fff',
                    // width:
                    borderRadius: 5,
                    marginBottom: 10,
                }}
            >

                <Text style={{color: '#111111', padding: 7, fontSize: 11}}>
                    {item.message}
                </Text>
                <Text
                    style={{
                        color: item.from === User.phone? '#42911e':'#bbbbbb',
                        padding: 3,
                        fontSize: 9,
                        alignSelf: 'flex-end'
                    }}>
                    {this.convertTime(item.time)}
                </Text>
            </View>
        )
    }

    render(){

        return(


                <ImageBackground
                    source={require('../images/telegramBackground.jpg')}
                    style={{flex: 1}}
                >
                    <SafeAreaView>
                    <FlatList
                        style={{padding: 10, height: height*0.8}}
                        data={this.state.messageList}
                        renderItem={this.renderRow}
                        keyExtractor={(item,index) => index.toString()}
                    />
                    <View
                        style={{
                            flexDirection: 'row',
                            alignItems: 'center',
                            justifyContent: 'center',
                            backgroundColor: '#fff',
                            width: '100%',
                            paddingVertical: 10,
                        }}>
                        <Image source={require('../images/smiling-emoticon-square-face.png')} style={{width: 20, height: 20,
                            marginRight: 20}}/>
                        <TextInput
                            style={styles.input}
                            value={this.state.textMessage}
                            placeholder={"Message"}
                            onChangeText={this.handleChange('textMessage')}
                        />
                        <TouchableOpacity onPress={this.sendMessage} style={{paddingBottom: 10, marginLeft: 5}}>
                            <Image source={require('../images/send-button.png')} style={{width: 20, height: 20,
                                marginLeft: 20}}/>
                        </TouchableOpacity>
                    </View>
                    </SafeAreaView>
                </ImageBackground>

        )
    }
}