import React from 'react';
import {
    ActivityIndicator,
    AsyncStorage,
    StatusBar,
    StyleSheet,
    View,
} from 'react-native';
import firebase from 'firebase';
import User from '../User'

export default class AuthLoadingScreen extends React.Component {
    constructor(props){
        super(props);
        this._bootstrapAsync();
    }

    componentWillMount(){
        var firebaseConfig = {
            apiKey: "AIzaSyBcwh7FRVgXdJHyz-rLRbauDwxc2Jw9XE4",
            authDomain: "fir-chat-385f6.firebaseapp.com",
            databaseURL: "https://fir-chat-385f6.firebaseio.com",
            projectId: "fir-chat-385f6",
            storageBucket: "fir-chat-385f6.appspot.com",
            messagingSenderId: "278415723728",
            appId: "1:278415723728:web:e876bedecdd8bf6d",
        };
        //Initialize Firebase
        firebase.initializeApp(firebaseConfig);
    }
    // Fetch the token from storage the navigate to our appropriate place
    _bootstrapAsync = async () => {
        User.phone = await AsyncStorage.getItem('userPhone');

        this.props.navigation.navigate(User.phone? 'App': 'Auth');
    };

    render() {
        return (
            <View>
                <ActivityIndicator/>
                <StatusBar barStyle="default" />
            </View>
        )
    }

}