import React, {Component} from 'react';
import {
    SafeAreaView,
    View,
    Image,
    Text,
    TouchableOpacity,
    AsyncStorage,
    FlatList,
} from 'react-native';
import User from '../User'
import styles from '../constants/styles'
import firebase from 'firebase'
import Entypo from 'react-native-vector-icons/Entypo'
import AntDesign from 'react-native-vector-icons/AntDesign'

const menuIcon = <Entypo name="menu" size={30} color="#fff" />;
const searchIcon = <AntDesign name="search1" size={20} color="#fff" />;
var colorCode = 'rgb(' + (Math.floor(Math.random() * 256)) + ',' + (Math.floor(Math.random() * 256)) + ',' + (Math.floor(Math.random() * 256 )) + ')';
export default class HomeScreen extends React.Component{
    static navigationOptions = ({navigation}) => {
        return {
            title: 'Telegram',
            headerStyle: {
                backgroundColor: '#59A6DD'
            },
            headerTintColor: '#fff',
            headerRight: (
                <TouchableOpacity
                    style={{marginRight: 10}}
                    onPress={()=> navigation.navigate('Profile')}>
                    {searchIcon}
                </TouchableOpacity>
            ),
            headerLeft: (
                <TouchableOpacity
                    style={{marginLeft: 10}}
                    onPress={()=> navigation.navigate('Profile')}>
                    {menuIcon}
                </TouchableOpacity>
            )
        }
    }

    state = {
        users: []
    }

    componentWillMount(){
        let dbRef = firebase.database().ref('users')
        dbRef.on('child_added', (val) => {
            let person = val.val();
            person.phone = val.key;
            if(person.phone === User.phone){
                User.name = person.name
            }else{
                this.setState((prevState) => {
                    return {
                        users: [...prevState.users, person]
                    }
                })
            }
        })
    }

    renderRow = ({item}) => {
        return (
            <TouchableOpacity
                onPress={() => this.props.navigation.navigate('Chat', item)}
                style={{
                    alignItems: 'center',
                    flexDirection: 'row',
                    padding: 10,
                    borderBottomColor: '#cecece',
                    borderBottomWidth: 1}}
            >
                <View style={styles.avatarIcon}>
                    <Text style={{
                        color: '#fff',
                        fontSize: 18,
                    }}>{item.name[0]}</Text>
                </View>
                    <View>
                        <Text style={{fontSize: 20}}>{item.name}</Text>
                    </View>
            </TouchableOpacity>
        )
    }
    render(){
        return(
            <SafeAreaView style = {styles.container}>
                <FlatList
                    style={{width: '100%'}}
                    data = {this.state.users}
                    renderItem = {this.renderRow}
                    keyExtractor={(item)=> item.phone}
                />
            </SafeAreaView>
        )
    }

}